/**
 * @file
 * Custom JS fixed vbo.
 */
(function ($, Drupal, drupalSettings, once) {
  'use strict';
  $( document ).ready(function() {
    $(once('select_all','th.select-all input')).on('click', function () {
      let isChecked = $(this).prop('checked');
      $(this).closest('table').find('.js-vbo-checkbox').prop('checked', isChecked);
      toggleButtonsState();
    });
    $(once('vbo_check','.js-vbo-checkbox')).on('click', function (){
      toggleButtonsState();
    });
    $(once('edit_action','select#edit-action')).on('change', function () {
      toggleButtonsState();
    });
  });

  function toggleButtonsState() {
    let anyItemsSelected = false;
    $('.js-vbo-checkbox').each(function () {
      if ($(this).prop('checked')) {
        anyItemsSelected = true;
        return false;
      }
    });
    let actionSelect = false;
    if ($('#edit-action').val() !== '') {
      actionSelect = true;
    }
    let has_selection = false;
    if(anyItemsSelected && actionSelect) {
      has_selection = true;
    }
    let buttons = $('input[data-vbo="vbo-action"], button[data-vbo="vbo-action"]');
    buttons.prop('disabled', !has_selection);
  }

})(jQuery, Drupal, drupalSettings, once);
