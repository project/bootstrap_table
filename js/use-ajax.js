/**
 * @file
 * Custom JS fixed use-ajax.
 */
(function ($, Drupal, once) {
  'use strict';
  $(document).ready(function () {
    $(once('useajax', 'table .use-ajax')).on('click', function (event) {
      event.preventDefault();
      const $linkElement = $(this);
      const elementSettings = {
        progress: {type: 'throbber'},
        dialogType: $linkElement.data('dialog-type'),
        dialog: $linkElement.data('dialog-options'),
        dialogRenderer: $linkElement.data('dialog-renderer'),
        base: $linkElement.attr('id'),
        element: this,
      };

      const href = $linkElement.attr('href');
      if (href) {
        elementSettings.url = href;
        elementSettings.event = 'click';
      }

      const httpMethod = $linkElement.data('ajax-http-method');
      if (httpMethod) {
        elementSettings.httpMethod = httpMethod;
      }
      let ajaxObject = Drupal.ajax(elementSettings);
      ajaxObject.execute();
    });
  });

})(jQuery, Drupal, once);
